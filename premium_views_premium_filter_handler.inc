<?php

/**
 * @file
 * Filter by whether a node is premium or not.
 */

class premium_views_premium_filter_handler extends views_handler_filter_boolean_operator {

  function construct() {
    parent::construct();
    $this->value_value = t('Is Premium');
  }
  
  function query() {
    $this->ensure_my_table();
    
    if ($this->value == 1) {
      $time = time();
      
      $this->query->add_where($this->options['group'], "(
      ($this->table_alias.start_ts = 0 and $this->table_alias.end_ts > $time) OR
      ($this->table_alias.start_ts < $time AND $this->table_alias.end_ts = 0) OR
      ($this->table_alias.start_ts = 0 AND $this->table_alias.end_ts = 0))");
    }
    else {
      $this->query->add_where($this->options['group'], "$this->table_alias.start_ts is NULL AND $this->table_alias.end_ts  is NULL");
    }
  }
}

