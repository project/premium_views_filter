<?php

/**
 * @file
 * register the premium views filter handlers.
 */

function premium_views_filter_views_data_alter(&$data) {
  $data['premium']['table']['group']  = t('Premium');

  $data['premium']['table']['base'] = array(
    'field' => 'premium',
    'title' => t('Premium'),
    'help' => t('Nodes flagged as premium content.'),
  );

  $data['premium']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
      'type' => 'LEFT', // not all nodes are premium
    ),
  );

  $data['premium']['nid'] = array(
    'title' => t('Premium'),
    'help' => t('Select nodes that are premium content'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'premium_views_premium_filter_handler',
      'label' => t('Is Premium Content'),
      'type' => 'yes-no',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
}

/**
 * Implementation of hook_views_handlers
 */
function premium_views_filter_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'premium_views_filter'),
    ),
    'handlers' => array(
      'premium_views_premium_filter_handler' => array(
        'parent' => 'views_handler_filter_boolean_operator',
      ),
    ),
  );
}


